# artanis-org-site

New website for GNU artanis using [ReadTheOrg](https://github.com/fniessen/org-html-themes#readtheorg) template.

# Requirements

- Emacs with Org Mode

# Building

- Open `artanis.org` in emacs

- when finishing editing, you can export the file to html with `org-html-export-to-html`

- Exported `artanis.html` file resides in the same directory, you can open it in a web browser of your choice & preview
